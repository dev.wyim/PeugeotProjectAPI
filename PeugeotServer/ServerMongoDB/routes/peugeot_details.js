var express = require('express');
var router = express.Router();

var mongoose = require('mongoose');
var peugeot_schema = require('../models/peugeot_schema.js');
var dataJSON;

/* GET listing. */
router.get('/all', function(req, res, next) {
  peugeot_schema.find(function (err, data) {
      if (err) return next(err);
      res.json(data);
    });
});

/* CREATE data. */
router.post('/add', function(req, res, next) {
  peugeot_schema.create(req.body, function(err, post) {
    if (err) {
      return next(err);
    } else {
      res.json(post);
    }
  });
});

/* UPDATE data. */
router.put('/update/:id', function(req, res, next) {
  peugeot_schema.findByIdAndUpdate(req.params.id, req.body, function(err, post) {
    if (err) {
      return next(err);
    } else {
      res.json(post);
    }
  });
});

/* DELETE data. */
router.delete('/delete/:id', function(req, res, next) {
  peugeot_schema.findByIdAndRemove(req, params.id, req.body, function(err, post) {
    if (err) {
      return next(err);
    } else {
      res.json(post);
    }
  });
});

module.exports = router;
